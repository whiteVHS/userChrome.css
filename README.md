[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Explanation
This is my personal, minimal stylesheet for Firefox-based browsers (I use this with LibreWolf).

## Installation

Enable the option `toolkit.legacyUserProfileCustomizations.stylesheets` from your `about:config` or `user.js` file. (If you're using LibreWolf, you can allow userChrome.css customization from the LibreWolf preferences in Settings)

Then open your profile folder (you can find the location in `about:profiles`), create the folder `chrome` inside of it, and place the `userChrome.css` file inside.

Preferably used with a Dark theme (I use High Contrast Black) and Compact density (enabled with the option `browser.compactmode.show` in `about:config`).

## Credits
Based off [Eric Murphy's userChrome.css file](https://github.com/ericmurphyxyz/userChrome.css) (which is based off [FirefoxCSS-Darknight](https://github.com/BriLHR/FirefoxCSS-Darknight) and [keyfox](https://github.com/AlfarexGuy2019/keyfox/), with additional lines from [Cascade](https://github.com/andreasgrafen/cascade).)
